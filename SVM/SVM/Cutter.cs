﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SVM
{
    class Cutter
    {
        int length;
        public Cutter(int length)
        {
            this.length = length;
        }

        /**
		 * 将二值化的图片进行切割，提取出只包含验证码的部分矩阵
		 * @param matrix 二值化矩阵
		 * @return List<Matrix> 一般为4个切割完成的矩阵
		 * 使用方法：生成Cutter对象，然后调用此方法。
		 */
        public List<Matrix> Cut(Matrix m)
        {
            int[] ColSplits;
            int[] RowSplits;
            ColSplits = this.getColSplits(m);
            RowSplits = this.getRowSplits(m, ColSplits);
            List<Matrix> matrixes = this.getMatrixes(m, ColSplits, RowSplits);
            //将分割出来的四个矩阵打印出来。
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine(matrixes.ElementAt(i).ToString());
            }
            return matrixes;
        }

        private List<Matrix> getMatrixes(Matrix m, int[] ColSplits, int[] RowSplits) 
        {
            List<Matrix> matrixes = new List<Matrix>();
            for (int i=0; i < ColSplits.Length/2; i++)
            {
                int Xcenter = (ColSplits[2*i] + ColSplits[2*i+1]) / 2;
                Console.WriteLine("Xcenter = " + Xcenter);
                int Ycenter = (RowSplits[2*i] + RowSplits[2*i+1]) / 2;
                Console.WriteLine("Ycenter = " + Ycenter);
                if(((RowSplits[2*i + 1] - RowSplits[2 * i]) > 3) || (RowSplits[2 * i + 1] - RowSplits[2 * i])==0)
                {
                    matrixes.Add(this.getMatrix(m, Xcenter, Ycenter, ColSplits, RowSplits, i));
                }
                if (matrixes.Count == 4) break;
            }
            return matrixes;
        }

        /**
		 * 得到列方向上的一般为8条的分割线
		 * @param matrix 二值化矩阵
		 * @return int[] 一个存有列索引的int数组
		 */
        private int[] getColSplits(Matrix matrix)
        {
            int[] ColSplits = new int[12];
            int[] sums = new int[matrix.Col];
            for (int i = 0; i < matrix.Col; i++)
            {
                for (int j = 0; j < matrix.Row; j++)
                {
                    sums[i] += Convert.ToInt32(matrix[j, i]);
                }
            }
            int n = 0;
            if (sums[0] != 0)
            {
                ColSplits[0] = 0;
                n++;
            }
            for (int i = 1; i < sums.Length - 1; i++)
            {
                if ((sums[i] != 0 && sums[i - 1] == 0) || (sums[i] != 0 && sums[i + 1] == 0))
                {
                    ColSplits[n] = i;
                    n++;
                }
            }
            if (sums[sums.Length - 1] != 0)
            {
                ColSplits[n] = sums.Length - 1;
                n++;
            }

            //对记录下来的噪点进行清理。
            for (int i = 0; i < ColSplits.Length / 2; i++)
            {
                if ((ColSplits[2 * i + 1] - ColSplits[2 * i] <= 2) && (ColSplits[2 * i + 1] != 0) && (ColSplits[2 * i] != 0))
                {
                    ColSplits[2 * i + 1] = 0;
                    ColSplits[2 * i] = 0;
                    for (int k = 0; (2 * i + 2 * k + 2) < 10; k++)
                    {
                        ColSplits[2 * i + 2 * k] = ColSplits[2 * i + 2 * k + 2];
                        ColSplits[2 * i + 1 + 2 * k] = ColSplits[2 * i + 1 + 2 * k + 2];
                    }
                }
            }

            //对colsplits相差超过25的进行分割
            for (int i = 0; i < ColSplits.Length / 2; i++)
            {
                if (ColSplits[2 * i + 1] - ColSplits[2 * i] > 25)
                {
                    for (int k = ColSplits.Length - 1; k >= 2 * i + 3; k--)
                    {
                        ColSplits[k] = ColSplits[k - 2];
                    }
                    ColSplits[2 * i + 1] = ColSplits[2 * i + 2] = (ColSplits[2 * i] + ColSplits[2 * i + 3]) / 2;
                    n = n + 2;
                }
            }

            //处理粘连问题。
            if (n < 8)
            {
                if (n == 6)
                {
                    int a = ColSplits[1] - ColSplits[0];
                    int b = ColSplits[3] - ColSplits[2];
                    int c = ColSplits[5] - ColSplits[4];
                    int max = a;
                    if (b > max) max = b;
                    if (c > max) max = c;
                    if (max == a)
                    {
                        for (int i = 7; i >= 2; i--)
                        {
                            ColSplits[i] = ColSplits[i - 2];
                        }
                        ColSplits[1] = ColSplits[2] = (ColSplits[0] + ColSplits[3]) / 2;
                    }
                    if (max == b)
                        {
                            for (int i = 7; i >= 5; i--)
                            {
                                ColSplits[i] = ColSplits[i - 2];
                            }
                            ColSplits[3] = ColSplits[4] = (ColSplits[2] + ColSplits[5]) / 2;
                        }
                        if (max == c)
                        {
                            ColSplits[7] = ColSplits[5];
                            ColSplits[5] = ColSplits[6] = (ColSplits[4] + ColSplits[7]) / 2;
                        }
                    }
                //两两粘连的情况
                if (n == 4)  
                    {
                        int a = ColSplits[1] - ColSplits[0];
                        int b = ColSplits[3] - ColSplits[2];
                        int c = ColSplits[5] - ColSplits[4];
                        int d = ColSplits[7] - ColSplits[6];
                        int max = a;
                        if (b > max) max = b;
                        if (c > max) max = c;
                        if (d > max) max = d;
                    }
                }
            return ColSplits;
        }

        /**
		 * 得到行方向上的几个分界线数值
		 * @param matrix 灰度化的Bitmap对象
         * ColSplits 前面已经得到的列分割索引数组
		 * @return int[] 行分割索引数组
		 */
        private int[] getRowSplits(Matrix matrix, int[] ColSplits)
        {
            int[] RowSplits = new int[12];
            int nn = 0;
            for (int n = 0; n < RowSplits.Length/2; n++)
            {
                int[] sums = new int[matrix.Row];
                for (int i = 0; i < matrix.Row; i++)
                {
                    for (int j = ColSplits[n * 2]; j <= ColSplits[n * 2 + 1]; j++)
                    {
                        sums[i] += Convert.ToInt32(matrix[i, j]);
                    }
                }
                for (int i = 0; i < sums.Length-4; i++)
                {
                    if (sums[0] != 0 && sums[3]!=0)
                    {
                        RowSplits[nn] = 0;
                        nn++;
                        break;
                    }
                    if (sums[i] == 0 && sums[i + 1] != 0 && sums[i + 3] != 0)
                    {
                        RowSplits[nn] = i + 1;
                        nn++;
                        break;
                    }
                }
                for (int i = matrix.Row-1; i >3; i--)
                {
                    if (sums[matrix.Row - 1] != 0 && sums[matrix.Row -4]!=0)
                    {
                        RowSplits[nn] = matrix.Row - 1;
                        nn++;
                        break;
                    }
                    if (sums[i] == 0 && sums[i - 1] != 0 && sums[i - 3] != 0)
                    {
                        RowSplits[nn] = i - 1;
                        nn++;
                        break;
                    }
                }
            }
            return RowSplits;
        }

        /**
		 * 生成一个以某字母为中心的特定大小的矩阵
		 * @param matrix 原矩阵
         * Xcenter X方向上的中点索引
         * Ycenter Y方向上的中点索引
         * ColSolits 列分割索引数组
         * RowSplits 行分割索引数组
         * n 一共生成几个矩阵
		 * @return M 特定大小的矩阵
		 */
        private Matrix getMatrix(Matrix m, int Xcenter, int Ycenter, int[] ColSplits, int[] RowSplits, int n)
        {
            int x1 = Xcenter - this.length/2, y1 = Ycenter - this.length/2;
            Matrix M = new Matrix(this.length);
            for (int i = 0; i < this.length; i++)
            {
                for(int j = 0; j < this.length; j++)
                {
                    if((x1+j)>=ColSplits[2*n] && (x1 + j) <= ColSplits[2 * n + 1] 
                        && (y1 + i) >= RowSplits[2*n] && (y1 + i) <= RowSplits[2 * n +1])
                    {
                        M[i, j] = m[y1 + i, x1 + j];
                    }
                    else
                    {
                        M[i, j] = 0;
                    }
                }
            }
            return M;
        }
    }
}
