﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * 作者：王宾鲁
 * 本类为SVM算法训练与测试
 */

namespace SVM
{
    class SVM
    {
        /// <summary>
        ///     通过训练集输出分界函数参数w和alpha和b
        /// </summary>
        /// <param name="C">惩罚系数</param>
        /// <param name="toler">松弛变量</param>
        /// <param name="kT">核函数类型</param>
        /// <param name="oneToOne">是否是一对一型比较算法</param>
        /// <param name="sigma">高斯核函数参数值</param>
        public static void produceWsBsAs(double C, double toler, kernelType kT, bool oneToOne, double sigma=10)
        {
            if (kT == kernelType.lin)
            {
                if (oneToOne)
                {
                    // 生成分类函数参数w和b
                    FileStream wsfile = new FileStream("ws_lin_oneone.txt", FileMode.Create);
                    FileStream bsfile = new FileStream("bs_lin_oneone.txt", FileMode.Create);
                    //List<int>[] svs = new List<int>[36];
                    //for (int i = 0; i < 36; i++)
                    //{
                    //    svs[i] = new List<int>();
                    //}
                    for (int i = 0; i < 36; i++)
                    {
                        for (int j = i + 1; j < 36; j++)
                        {
                            Console.WriteLine("_________{0}_________{1}__________", i, j);
                            List<Matrix> matrixes = LetterMatrixForTrain.GetTrainByTwoMatrix(i, j);
                            Matrix data = matrixes.ElementAt(0);
                            Matrix label = matrixes.ElementAt(1);

                            double b = 0;
                            Matrix a = smoP(data, label, C, toler, 100, kT, sigma, ref b);
                            //for (int t = 0; t < a.Row; t++)
                            //{
                            //    if (a[t, 0] > 0.001)
                            //    {
                            //        if (label[t, 0] > 0)
                            //        {
                            //            svs[i].Add(t);
                            //        }
                            //        else
                            //        {
                            //            svs[j].Add(t);
                            //        }
                            //    }
                            //}
                            Matrix w = calcWs(a, data, label);
                            byte[] wsdata = System.Text.Encoding.Default.GetBytes(w.Transpose().ToString());
                            wsfile.Write(wsdata, 0, wsdata.Length);
                            wsfile.Flush();
                            byte[] bsdata = System.Text.Encoding.Default.GetBytes(b + "\n");
                            bsfile.Write(bsdata, 0, bsdata.Length);
                            bsfile.Flush();
                        }
                    }
                    Console.WriteLine("finished!");
                    //FileStream svfile = new FileStream("svs.txt", FileMode.Create);
                    //for (int i = 0; i < 36; i++)
                    //{
                    //    svs[i].Sort();
                    //    int nownum = -1;
                    //    for (int t = 0; t < svs[i].Count; t++)
                    //    {
                    //        int sv = svs[i].ElementAt(t);
                    //        if (nownum != sv)
                    //        {
                    //            byte[] svdata = System.Text.Encoding.Default.GetBytes(sv + " ");
                    //            svfile.Write(svdata, 0, svdata.Length);
                    //        }
                    //        nownum = sv;
                    //    }
                    //    byte[] nextline = System.Text.Encoding.Default.GetBytes("\n");
                    //    svfile.Write(nextline, 0, nextline.Length);
                    //    svfile.Flush();
                    //}
                    wsfile.Close();
                    bsfile.Close();
                    //svfile.Close();
                }
                else
                {
                    List<Matrix> allMatrix = new List<Matrix>();
                    int[] rowNum = new int[36];
                    for (int i = 0; i < 36; i++)
                    {
                        string name = string.Format("/train/{0}", i);
                        Matrix newMat = LetterMatrixForTrain.getLetterMatrix(name, i);
                        allMatrix.Add(newMat);
                        if (i == 0)
                        {
                            rowNum[i] = newMat.Row;
                        }
                        else
                        {
                            rowNum[i] = newMat.Row + rowNum[i - 1];
                        }
                    }
                    Console.WriteLine("read finish!");
                    Matrix totalM = new Matrix(rowNum[35], 625);
                    Matrix totalLabel = new Matrix(rowNum[35], 1);
                    int k = 0;
                    for (int t = 0; t < 36; t++)
                    {
                        for (int i = 0; i < rowNum[t] - ((t == 0) ? 0 : rowNum[t - 1]); i++)
                        {
                            for (int j = 0; j < 625; j++)
                            {
                                totalM[k, j] = allMatrix.ElementAt(t)[i, j];
                            }
                            totalLabel[k, 0] = -1;
                            k++;
                        }
                    }
                    FileStream wsfile = new FileStream("ws_lin_oneall.txt", FileMode.Create);
                    FileStream bsfile = new FileStream("bs_lin_oneall.txt", FileMode.Create);
                    for (int i = 14; i < 15; i++)
                    {
                        Console.WriteLine("--------{0}---------", i);
                        for (int t = ((i == 0) ? 0 : rowNum[i - 1]); t < rowNum[i]; t++)
                        {
                            totalLabel[t, 0] = 1;
                        }
                        double b = 0;
                        Matrix alphas = smoP(totalM, totalLabel, C, toler, 100, kT, sigma, ref b);
                        Matrix w = calcWs(alphas, totalM, totalLabel);
                        byte[] wsdata = System.Text.Encoding.Default.GetBytes(w.Transpose().ToString());
                        wsfile.Write(wsdata, 0, wsdata.Length);
                        wsfile.Flush();
                        byte[] bsdata = System.Text.Encoding.Default.GetBytes(b + "\n");
                        bsfile.Write(bsdata, 0, bsdata.Length);
                        bsfile.Flush();
                        Matrix bs = new Matrix(totalM.Row, 1);
                        bs.SetValue(b);
                        Matrix result = Matrix.Multiply(totalM * w + bs, totalLabel);
                        double trueNum = 0;
                        for (int t = 0; t < totalM.Row; t++)
                        {
                            if (result[t, 0] > 0)
                            {
                                trueNum++;
                            }
                            Console.WriteLine(result[t, 0] > 0);
                        }
                        Console.WriteLine("rate:{0}", trueNum / totalM.Row);
                        for (int t = ((i == 0) ? 0 : rowNum[i - 1]); t < rowNum[i]; t++)
                        {
                            totalLabel[t, 0] = -1;
                        }
                    }
                    wsfile.Close();
                    bsfile.Close();
                }
            }
            else
            {
                if (oneToOne)
                {
                    // 生成分类函数参数a和b
                    FileStream asfile = new FileStream("as_rbf_oneone.txt", FileMode.Create);
                    FileStream bsfile = new FileStream("bs_rbf_oneone.txt", FileMode.Create);
                    for (int i = 0; i < 36; i++)
                    {
                        for (int j = i + 1; j < 36; j++)
                        {
                            Console.WriteLine("_________{0}_________{1}__________", i, j);
                            List<Matrix> matrixes = LetterMatrixForTrain.GetTrainByTwoMatrix(i, j);
                            Matrix data = matrixes.ElementAt(0);
                            Matrix label = matrixes.ElementAt(1);

                            double b = 0;
                            Matrix a = smoP(data, label, C, toler, 100, kT, sigma, ref b);

                            Matrix w = calcWs(a, data, label);
                            byte[] asdata = System.Text.Encoding.Default.GetBytes(Matrix.Multiply(a, label).Transpose().ToString());
                            asfile.Write(asdata, 0, asdata.Length);
                            asfile.Flush();
                            byte[] bsdata = System.Text.Encoding.Default.GetBytes(b + "\n");
                            bsfile.Write(bsdata, 0, bsdata.Length);
                            bsfile.Flush();
                        }
                    }
                    Console.WriteLine("finished!");
                    asfile.Close();
                    bsfile.Close();
                }
                else
                {
                    List<Matrix> allMatrix = new List<Matrix>();
                    int[] rowNum = new int[36];
                    for (int i = 0; i < 36; i++)
                    {
                        string name = string.Format("/train/{0}", i);
                        Matrix newMat = LetterMatrixForTrain.getLetterMatrix(name, i);
                        allMatrix.Add(newMat);
                        if (i == 0)
                        {
                            rowNum[i] = newMat.Row;
                        }
                        else
                        {
                            rowNum[i] = newMat.Row + rowNum[i - 1];
                        }
                    }
                    Console.WriteLine("read finish!");
                    Matrix totalM = new Matrix(rowNum[35], 625);
                    Matrix totalLabel = new Matrix(rowNum[35], 1);
                    int k = 0;
                    for (int t = 0; t < 36; t++)
                    {
                        for (int i = 0; i < rowNum[t] - ((t == 0) ? 0 : rowNum[t - 1]); i++)
                        {
                            for (int j = 0; j < 625; j++)
                            {
                                totalM[k, j] = allMatrix.ElementAt(t)[i, j];
                            }
                            totalLabel[k, 0] = -1;
                            k++;
                        }
                    }
                    // 生成分类函数参数a和b
                    FileStream asfile = new FileStream("as_rbf_oneall.txt", FileMode.Create);
                    FileStream bsfile = new FileStream("bs_rbf_oneall.txt", FileMode.Create);
                    for (int i = 27; i < 28; i++)
                    {
                        Console.WriteLine("--------{0}---------", i);
                        for (int t = ((i == 0) ? 0 : rowNum[i - 1]); t < rowNum[i]; t++)
                        {
                            totalLabel[t, 0] = 1;
                        }
                        double b = 0;
                        Matrix a = smoP(totalM, totalLabel, C, toler, 100, kT, sigma, ref b);

                        Matrix w = calcWs(a, totalM, totalLabel);
                        byte[] asdata = System.Text.Encoding.Default.GetBytes(Matrix.Multiply(a, totalLabel).Transpose().ToString());
                        asfile.Write(asdata, 0, asdata.Length);
                        asfile.Flush();
                        byte[] bsdata = System.Text.Encoding.Default.GetBytes(b + "\n");
                        bsfile.Write(bsdata, 0, bsdata.Length);
                        bsfile.Flush();
                        double trueNum = 0;
                        for (int t = 0; t < totalM.Row; t++)
                        {
                            double fresult = (Matrix.Multiply(a, totalLabel).Transpose().getRow(0) * OptStruct.kernelTrans(totalM, totalM.getRow(t), kernelType.rbf, 10)).ToDouble() + b;
                            if (fresult > 0)
                            {
                                trueNum++;
                            }
                            Console.WriteLine(fresult > 0);
                        }
                        Console.WriteLine("rate:{0}", trueNum / totalM.Row);

                        for (int t = ((i == 0) ? 0 : rowNum[i - 1]); t < rowNum[i]; t++)
                        {
                            totalLabel[t, 0] = -1;
                        }
                    }
                    asfile.Close();
                    bsfile.Close();
                }
            }
        }

        /// <summary>
        ///     对两个字母准确性作出测试
        /// </summary>
        /// <param name="C">惩罚系数</param>
        /// <param name="toler">松弛变量</param>
        /// <param name="kT">核函数类型</param>
        /// <param name="sigma">高斯核函数参数</param>
        /// <param name="digitA">第一个字母（字母为0-9A-Z，分别用数字0-35代替）</param>
        /// <param name="digitB">第二个字母</param>
        public static void testTwoDigits(double C, double toler, kernelType kT, double sigma, int digitA, int digitB)
        {
            List<Matrix> matrixes = LetterMatrixForTrain.GetTrainByTwoMatrix(digitA, digitB);
            Matrix data = matrixes.ElementAt(0);
            Matrix label = matrixes.ElementAt(1);
            Matrix test = matrixes.ElementAt(2);

            double b = 0;
            Matrix a = smoP(data, label, C, toler, 100, kT, sigma, ref b);
            int svms = 0;
            for (int i = 0; i < data.Row; i++)
            {
                if (a[i, 0] > 0)
                {
                    svms++;
                }
            }
            Console.WriteLine("sv numbers:{0}", svms);
            Matrix w = calcWs(a, data, label);
            Matrix bs = new Matrix(data.Row, 1);
            bs.SetValue(b);
            Matrix result = Matrix.Multiply(data * w + bs, label);
            
            double trueNum = 0;
            double trueRate = 0;
            for (int i = 0; i < data.Row; i++)
            {
                if (kT == kernelType.rbf)
                {
                    if (((OptStruct.kernelTrans(data, data.getRow(i), kernelType.rbf, 10).Transpose() * Matrix.Multiply(a, label)).ToDouble() + b) * label[i, 0] > 0)
                    {
                        trueNum += 1;
                    }
                    Console.WriteLine(((OptStruct.kernelTrans(data, data.getRow(i), kernelType.rbf, 10).Transpose() * Matrix.Multiply(a, label)).ToDouble() + b) * label[i, 0] > 0);
                }
                else
                {
                    if (result[i, 0] > 0)
                    {
                        trueNum += 1;
                    }
                    Console.WriteLine(result[i, 0] > 0);
                }
            }
            trueRate = trueNum / data.Row;
            Console.WriteLine("正确率{0}", trueRate);
            // 测试集结果
            Matrix testb = new Matrix(test.Row, 1);
            testb.SetValue(b);
            Matrix result2 = test * w + testb;
            if (kT == kernelType.lin)
            {
                for (int i = 0; i < result2.Row; i++)
                {
                    Console.WriteLine(result2[i,0]);
                }
            }
            else
            {
                for (int i = 0; i < result2.Row; i++)
                {
                    Console.WriteLine((OptStruct.kernelTrans(data, test.getRow(i), kernelType.rbf, 10).Transpose() * Matrix.Multiply(a, label)).ToDouble() + b);
                }
            }
            
        }

        /// <summary>
        ///     无优化alpha对选择的简易版训练函数，仅用于测试
        /// </summary>
        /// <param name="dataMatIn">训练集矩阵</param>
        /// <param name="classLabels">训练集标记</param>
        /// <param name="C">惩罚系数</param>
        /// <param name="toler">松弛变量</param>
        /// <param name="maxIter">最大循环数</param>
        /// <param name="b">分解函数参数</param>
        /// <returns>拉格朗日因子alphas</returns>
        public static Matrix smoSimple(Matrix dataMatIn, Matrix classLabels, double C, double toler, int maxIter, ref double b)
        {
            b = 0;
            int m = dataMatIn.Row;
            int n = dataMatIn.Col;
            Matrix alphas = new Matrix(m, 1); 
            int iter = 0;
            while (iter < maxIter)
            {
                int alphaPairsChanged = 0;
                for(int i=0;i< m; i++)
                {
                    Matrix Xk = new Matrix(n, 1);
                    for (int t = 0; t < n; t++)
                    {
                        Xk[t, 0] = dataMatIn[i, t];
                    }
                    double fXi = ((Matrix.Multiply(alphas, classLabels)).Transpose() * (dataMatIn * Xk)).ToDouble() + b;
                    double Ei = fXi - classLabels[i,0];
                    if (((classLabels[i, 0] * Ei < -toler) && (alphas[i, 0] < C)) ||
                            (classLabels[i, 0] * Ei > toler) && (alphas[i, 0] > 0))
                    {
                        int j = selectJRand(i, m);
                        for (int t = 0; t < n; t++)
                        {
                            Xk[t, 0] = dataMatIn[j, t];
                        }
                        double fXj = ((Matrix.Multiply(alphas, classLabels)).Transpose() * (dataMatIn * Xk)).ToDouble() + b;
                        double Ej = fXj - classLabels[j, 0];
                        double alphaIold = alphas[i, 0];
                        double alphaJold = alphas[j, 0];
                        double L = 0, H = 0 ;
                        if (classLabels[i, 0] != classLabels[j, 0])
                        {
                            L = Math.Max(0, alphas[j, 0] - alphas[i, 0]);
                            H = Math.Min(C, C + alphas[j, 0] - alphas[i, 0]);
                        }
                        else
                        {
                            L = Math.Max(0, alphas[j, 0] + alphas[i, 0] - C);
                            H = Math.Min(C, alphas[j, 0] + alphas[i, 0]);
                        }
                        if (L == H)
                        {
                            Console.WriteLine("L==H");
                            continue;
                        }
                        double eta = 0;
                        for (int q = 0; q < dataMatIn.Col; q++)
                        {
                            eta += 2 * dataMatIn[i, q] * dataMatIn[j, q] - dataMatIn[i, q] * dataMatIn[i, q] - dataMatIn[j, q] * dataMatIn[j, q];
                        }
                        if (eta >= 0)
                        {
                            Console.WriteLine("eta>=0");
                            continue;
                        }
                        alphas[j, 0] -= classLabels[j, 0] * (Ei - Ej) / eta;
                        alphas[j, 0] = clipAlpha(alphas[j, 0], H, L);
                        if (Math.Abs(alphas[j, 0] - alphaJold) < 0.00001)
                        {
                            Console.WriteLine("j not moving enough");
                            continue;
                        }
                        alphas[i, 0] += classLabels[j, 0] * classLabels[i, 0] * (alphaJold - alphas[j, 0]);
                        double b1 = 0, b2 = 0;
                        double XiXi = 0;
                        double XjXj = 0;
                        double XiXj = 0;
                        for (int p = 0; p < n; p++)
                        {
                            XiXi += dataMatIn[i, p] * dataMatIn[i, p];
                            XjXj += dataMatIn[j, p] * dataMatIn[j, p];
                            XiXj += dataMatIn[i, p] * dataMatIn[j, p];
                        }
                        b1 = b - Ei - classLabels[i, 0] * (alphas[i, 0] - alphaIold) * XiXi - classLabels[j, 0] * (alphas[j, 0] - alphaJold) * XiXj;
                        b2 = b - Ej - classLabels[i, 0] * (alphas[i, 0] - alphaIold) * XiXj - classLabels[j, 0] * (alphas[j, 0] - alphaJold) * XjXj;
                        if (alphas[i, 0] > 0 && alphas[i, 0] < C)
                        {
                            b = b1;
                        }
                        else if(alphas[j, 0] > 0 && alphas[j, 0] < C)
                        {
                            b = b2;
                        }
                        else
                        {
                            b = (b1 + b2) / 2;
                        }
                        alphaPairsChanged += 1;
                        Console.WriteLine("iter: {0} i:{1}, pairs changed {2}", iter, i, alphaPairsChanged);
                    }
                    
                }
                if (alphaPairsChanged == 0)
                {
                    iter += 1;
                }
                else
                {
                    iter = 0;
                }
                Console.WriteLine("iteration number: {0}", iter);
            }
            return alphas;
        }

        /// <summary>
        ///     在每次优化alpha对中随机选择待优化的另一个alpha
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <param name="os"></param>
        /// <param name="Ei"></param>
        /// <param name="Ej"></param>
        private static void selectJ(int i, ref int j, OptStruct os, double Ei, ref double Ej)
        {
            int maxK = -1;
            double maxDeltaE = 0;
            bool set = false;
            Ej = 0;
            os.eCache[i, 0] = 1;
            os.eCache[i, 1] = Ei;
            for (int t = 0; t < os.eCache.Row; t++)
            {
                if (t == i) continue;
                if (os.eCache[t, 0] > 0)
                {
                    // TODO
                    double Ek = calcEk(os, t);
                    double deltaE = Math.Abs(Ei - Ek);
                    if (deltaE > maxDeltaE)
                    {
                        set = true;
                        maxK = t;
                        maxDeltaE = deltaE;
                        Ej = Ek;
                    }
                }
            }
            if (set)
            {
                j = maxK;
            }
            else
            {
                j = selectJRand(i, os.m);
                Ej = calcEk(os, j);
            }
        }

        /// <summary>
        ///     smo算法内循环
        /// </summary>
        /// <param name="i"></param>
        /// <param name="os"></param>
        /// <returns></returns>
        private static int innerL(int i, OptStruct os)
        {
            double Ei = calcEk(os, i);
            if (((os.labelMat[i, 0] * Ei < -os.tol) && (os.alphas[i, 0] < os.C)) ||
                (os.labelMat[i, 0] * Ei > os.tol) && (os.alphas[i, 0] > 0))
            {
                int j = 0;
                double Ej = 0;
                selectJ(i, ref j, os, Ei, ref Ej);
                double alphaIold = os.alphas[i, 0];
                double alphaJold = os.alphas[j, 0];
                double L = 0, H = 0;
                if (os.labelMat[i, 0] != os.labelMat[j, 0])
                {
                    L = Math.Max(0, os.alphas[j, 0] - os.alphas[i, 0]);
                    H = Math.Min(os.C, os.C + os.alphas[j, 0] - os.alphas[i, 0]);
                }
                else
                {
                    L = Math.Max(0, os.alphas[j, 0] + os.alphas[i, 0] - os.C);
                    H = Math.Min(os.C, os.alphas[j, 0] + os.alphas[i, 0]);
                }
                if (L == H)
                {
                    Console.WriteLine("L==H");
                    return 0;
                }
                double eta = 2 * os.K[i, j] - os.K[i, i] - os.K[j, j] ;
                if (eta >= 0)
                {
                    Console.WriteLine("eta>=0");
                    return 0;
                }
                os.alphas[j, 0] -= os.labelMat[j, 0] * (Ei - Ej) / eta;
                os.alphas[j, 0] = clipAlpha(os.alphas[j, 0], H, L);
                updateEk(os, j);
                if (Math.Abs(os.alphas[j, 0] - alphaJold) < 0.00001)
                {
                    Console.WriteLine("j not moving enough");
                    return 0;
                }
                os.alphas[i, 0] += os.labelMat[j, 0] * os.labelMat[i, 0] * (alphaJold - os.alphas[j, 0]);
                updateEk(os, i);
                double b1 = 0, b2 = 0;
                b1 = os.b - Ei - os.labelMat[i, 0] * (os.alphas[i, 0] - alphaIold) * os.K[i,i] - os.labelMat[j, 0] * (os.alphas[j, 0] - alphaJold) * os.K[i,j];
                b2 = os.b - Ej - os.labelMat[i, 0] * (os.alphas[i, 0] - alphaIold) * os.K[i,j] - os.labelMat[j, 0] * (os.alphas[j, 0] - alphaJold) * os.K[j,j];
                if (os.alphas[i, 0] > 0 && os.alphas[i, 0] < os.C)
                {
                    os.b = b1;
                }
                else if(os.alphas[j, 0] > 0 && os.alphas[j, 0] < os.C)
                {
                    os.b = b2;
                }
                else
                {
                    os.b = (b1 + b2) / 2.0;
                }
                return 1;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        ///     SMO算法外循环
        /// </summary>
        /// <param name="dataMatIn">训练集矩阵</param>
        /// <param name="classLabels">训练集标记</param>
        /// <param name="C">惩罚系数</param>
        /// <param name="toler">松弛变量</param>
        /// <param name="maxIter">最大循环</param>
        /// <param name="kT">核函数类型</param>
        /// <param name="sigma">高斯核函数参数</param>
        /// <param name="b">分解函数参数</param>
        /// <returns>拉格朗日因子alphas</returns>
        private static Matrix smoP(Matrix dataMatIn, Matrix classLabels, double C, double toler, int maxIter, kernelType kT, double sigma, ref double b)
        {
            OptStruct os = new OptStruct(dataMatIn, classLabels, C, toler, kT, sigma);
            int iter = 0;
            bool entireSet = true;
            int alphaPairsChanged = 0;
            while ((iter < maxIter) && ((alphaPairsChanged > 0) || (entireSet)))
            {
                alphaPairsChanged = 0;
                if (entireSet)
                {
                    for(int i = 0; i < os.m; i++)
                    {
                        alphaPairsChanged += innerL(i, os);
                        Console.WriteLine("fullset, iter:{0} i:{1}, pairs changed {2}", iter, i, alphaPairsChanged);
                    }
                    
                    iter += 1;
                }
                else
                {
                    for(int i = 0; i < os.m; i++)
                    {
                        double alpha = os.alphas[i, 0];
                        if ((alpha > 0) && (alpha < C))
                        {
                            alphaPairsChanged += innerL(i, os);
                            Console.WriteLine("non-bound, iter: {0} i:{1}, pairs changed {2}", iter, i, alphaPairsChanged);
                        }
                    }
                    iter += 1;
                }
                if (entireSet) entireSet = false;
                else if (alphaPairsChanged == 0)
                {
                    entireSet = true;
                    Console.WriteLine("iteration number: {0}", iter);
                }
            }
            b = os.b;
            return os.alphas;
        }

        /// <summary>
        ///     计算分界函数参数w，仅用于线性分类
        /// </summary>
        /// <param name="alphas">拉格朗日因子</param>
        /// <param name="dataArr">训练集矩阵</param>
        /// <param name="classLabels">训练集标记</param>
        /// <returns></returns>
        private static Matrix calcWs(Matrix alphas, Matrix dataArr, Matrix classLabels)
        {
            int m = dataArr.Row;
            int n = dataArr.Col;
            Matrix w = new Matrix(n, 1);
            for(int i=0;i< m; i++)
            {
                for(int j=0;j< n; j++)
                {
                    w[j, 0] += alphas[i, 0] * classLabels[i, 0] * dataArr[i, j];
                }
            }
            return w;
        }

        /// <summary>
        ///     计算Ek
        /// </summary>
        /// <param name="os"></param>
        /// <param name="k"></param>
        /// <returns>Ek</returns>
        private static double calcEk(OptStruct os, int k)
        {
            Matrix Xk = new Matrix(os.X.Row, 1);
            for (int i = 0; i < os.X.Row; i++)
            {
                Xk[i, 0] = os.K[k, i];
            }
            double fXk = ((Matrix.Multiply(os.alphas, os.labelMat)).Transpose() * Xk).ToDouble() + os.b;
            double Ek = fXk - os.labelMat[k, 0];
            return Ek;
        }

        /// <summary>
        ///     更新Ek=fXk-yk
        /// </summary>
        /// <param name="os"></param>
        /// <param name="k"></param>
        private static void updateEk(OptStruct os, int k)
        {
            double Ek = calcEk(os, k);
            os.eCache[k, 0] = 1;
            os.eCache[k, 1] = Ek;
        }

        /// <summary>
        ///     通过上界和下界调整alpha
        /// </summary>
        /// <param name="aj">alphaj</param>
        /// <param name="H">上界</param>
        /// <param name="L">下界</param>
        /// <returns></returns>
        private static double clipAlpha(double aj, double H, double L)
        {
            if (aj > H) aj = H;
            if (aj < L) aj = L;
            return aj;
        }
        /// <summary>
        ///     在每次优化alpha对中随机选择待优化的另一个alpha
        /// </summary>
        /// <param name="i">已选择的alpha下标</param>
        /// <param name="m">alpha总数量</param>
        /// <returns></returns>
        private static int selectJRand(int i, int m)
        {
            Random random = new Random();
            int j = random.Next(0, m);
            while(j == i)
            {
                j = random.Next(0, m);
            }
            return j;
        }
    }
}
