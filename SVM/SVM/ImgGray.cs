﻿using System;
using System.Drawing;
/**
 * 作者：隗泽浩
 * 本类用于将图片二值化、去噪并将信息保存在矩阵内
 */
namespace SVM
{
    public class ImgGray
	{
		private Bitmap bitmap;

		/// <summary>
		/// 将图片灰度化，二值化，去噪，输出信息矩阵
		/// 下为生成Bitmap对象的语法
		/// string name = string.Format("/{0}.jpg", i);
		/// bitmap = new Bitmap(Image.FromFile(@Environment.CurrentDirectory + "/pic" + name));
		/// </summary>
		/// <returns>matrix 二值化去噪后的图片矩阵信息</returns>
		/// <param name="path">Path.图片路径</param>
		public Matrix processImg(String path)
		{
			bitmap = new Bitmap(Image.FromFile(path));
			Matrix matrix = ConvertToMatrix(ToGray(bitmap));
			return matrix;
		}

		/// <summary>
		/// 将彩色图片灰度化
		/// </summary>
		/// <returns>灰度化的Bitmap对象</returns>
		/// <param name="bmp">Bitmap对象</param>
		private Bitmap ToGray(Bitmap bmp)
		{
			for (int i = 0; i < bmp.Width; i++)
			{
				for (int j = 0; j < bmp.Height; j++)
				{
					//获取该点的像素的RGB的颜色 
					Color color = bmp.GetPixel(i, j);
					//利用公式计算灰度值 
					int gray = (int)(color.R * 0.3 + color.G * 0.59 + color.B * 0.11);
					Color newColor = Color.FromArgb(gray, gray, gray);
					bmp.SetPixel(i, j, newColor);
				}
			}
			return bmp;
		}

		 /// <summary>
		 /// 将灰度化的图片，二值化并去噪，返回信息矩阵
		 /// </summary>
		 /// <returns>matrix 二值化、去噪后的图片信息矩阵</returns>
		 /// <param name="bmp">灰度化的Bitmap对象</param>
		private Matrix ConvertToMatrix(Bitmap bmp)
		{
			Matrix matrix = new Matrix(bmp.Height, bmp.Width);
			int average = GetAverageGray(bmp);
			for (int i = 0; i < bmp.Width; i++)
			{
				for (int j = 0; j < bmp.Height; j++)
				{
					Color color = bmp.GetPixel(i, j);
					int newColor = color.B > average ? 0 : 1;
					matrix[j, i] = newColor;
				}
			}
			matrix = Denoice(matrix);
			return matrix;
		}

		/// <summary>
		/// 计算灰度化图片的平均灰度，用于二值化
		/// </summary>
		/// <returns>图片平均灰度</returns>
		/// <param name="bmp">灰度化的Bitmap对象</param>
		private int GetAverageGray(Bitmap bmp)
		{
			int average = 0;
			for (int i = 0; i < bmp.Width; i++)
			{
				for (int j = 0; j < bmp.Height; j++)
				{
					Color color = bmp.GetPixel(i, j);
					average += color.B;
				}
			}
			average = (int)average / (bmp.Width * bmp.Height);
			return average;
		}

		/// <summary>
		/// 为二值化后的矩阵去噪
		/// </summary>
		/// <returns>matrix 去噪后的图片矩阵</returns>
		/// <param name="matrix">二值化后的图片矩阵</param>
		private Matrix Denoice(Matrix matrix)
		{
			double sum;
			for(int t = 0; t < 3; t++)
            {
                for (int i = 0; i < matrix.Row; i++)
                {
                    for (int j = 0; j < matrix.Col; j++)
                    {
                        if (i == 0 || j == 0 || j == matrix.Col - 1)
                        {
                            matrix[i, j] = 0;
                        }
                        else if ((i == matrix.Row - 1) && (j != 0) && (j != matrix.Col - 1))
                        {
                            sum = matrix[i, j - 1] + matrix[i - 1, j] + matrix[i, j + 1];
                            if (sum < 2)
                            {
                                matrix[i, j] = 0;
                            }
                            sum = 0;
                        }
                        else
                        {
                            sum = matrix[i - 1, j - 1] + matrix[i - 1, j] + matrix[i - 1, j + 1]
                                + matrix[i, j - 1] + matrix[i, j + 1]
                                + matrix[i + 1, j - 1] + matrix[i + 1, j] + matrix[i + 1, j + 1];
                            if (sum < 3)
                            {
                                matrix[i, j] = 0;
                            }
                            sum = 0;
                        }
                    }
                }// end for
            }
			return matrix;
		}// end method
	}// end class
}// end namespace