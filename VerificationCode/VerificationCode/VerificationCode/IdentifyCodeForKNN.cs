﻿using System;
using System.IO;
using System.Collections.Generic;
/**
 * 作者：隗泽浩
 * 本类用于运行KNN算法
 */
namespace SVM
{
	public class IdentifyCodeForKNN
	{
		public static List<Matrix> trains = new List<Matrix>();

		public IdentifyCodeForKNN()
		{
		}

		/// <summary>
		/// 执行KNN算法来识别验证码图片
		/// </summary>
		/// <returns>识别后的结果</returns>
		/// <param name="path">被识别图片路径</param>
		public static String identifyLetter(String path)
		{
			List<String> code = new List<String>();
			ImgGray img = new ImgGray();
			Matrix matrix = img.processImg(path);
			Cutter cutter = new Cutter(25);
			List<Matrix> list = new List<Matrix>();

			foreach (Matrix mat in cutter.Cut(matrix))
			{
				list.Add(Rotate.chooseRotate(mat, 2, Math.PI / 18));
			}

			IdentifyCodeForKNN.getTrains();
			foreach (Matrix mat in list)
			{
				matrix = adjustMatrix(mat);
				KNN knn = new KNN(20, trains, matrix);
				code.Add(IdentifyCode.convertToLetter(knn.Classify())); 
			}
			String result = code[0] + code[1] + code[2] + code[3];
			return result;
		}

		/// <summary>
		/// 将矩阵化为行向量
		/// </summary>
		/// <returns>行向量</returns>
		/// <param name="matrix">原始矩阵</param>
		public static Matrix adjustMatrix(Matrix matrix)
		{
			Matrix ma = new Matrix(1, matrix.Row * matrix.Col);
			int k = 0;
			for (int i = 0; i < matrix.Row; i++)
			{
				for (int j = 0; j < matrix.Col; j++)
				{
					ma[0, k] = matrix[i, j];
					k++;
				}
			}
			return ma;
		}

		private static void getTrains()
		{
			for (int i = 0; i < 36; i++)
			{
				string name1 = string.Format("/train/{0}", i);
				// letter是标记，用于标记该矩阵的优先级，用以后面矩阵融合并生成标记矩阵
				int letter = i;
				getLetterMatrix(name1, letter);
			}
		}

		private static void getLetterMatrix(string folderName, int letter)
		{
 			string[] filePath = Directory.GetFiles(Environment.CurrentDirectory + folderName, "*.txt");

			foreach (string path in filePath)
			{
				Matrix matrix = new Matrix(1, 625);
				matrix.Flag = letter;
				StreamReader reader = new StreamReader(path);
				int j = 0;
				while (!reader.EndOfStream)
				{
					string[] line = reader.ReadLine().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
					for (var i = 0; i < line.Length; i++)
					{
						matrix[0, j] = Convert.ToInt32(line[i]);
						j++;
					}
				}
				trains.Add(matrix);
			}
		}
	}
}
