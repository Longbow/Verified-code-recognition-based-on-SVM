﻿using System;
using System.IO;
using System.Collections.Generic;
/**
 * 作者：隗泽浩
 * 本类用于为SVM算法生成训练集
 */
namespace SVM
{
	public class LetterMatrixForTrain
	{
		public const int COL = 625;
		public LetterMatrixForTrain()
		{
		}

		/// <summary>
		/// 用于展示如何把一个字母或数字的全部训练txt合为一个矩阵，再将两个字母和数字的矩阵合为一个矩阵
		/// 此函数根据实际使用需要进行调整
		/// </summary>
		/// <returns>The train by two matrix.</returns>
		/// <param name="i">The index.</param>
		/// <param name="j">J.</param>
		public static List<Matrix> GetTrainByTwoMatrix(int i, int j)
		{
			Matrix ma1 = null;
			Matrix ma2 = null;
            Matrix testm = null;

			/*以下仅为展示如何使用循环获得全部36个矩阵，没有实现对36个矩阵的存储*/

			// 获取表示数字的10个矩阵
			string name1 = string.Format("/history/10elementGause/train/{0}", i);
			// letter是标记，用于标记该矩阵的优先级，用以后面矩阵融合并生成标记矩阵
			int letter1 = i;
			ma1 = getLetterMatrix(name1, letter1);

			// 获取表示字母的26个矩阵
			string name2 = string.Format("/history/10elementGause/train/{0}", j);
			int letter2 = j;
			ma2 = getLetterMatrix(name2, letter2);

            // 用于测试的
            string name3 = string.Format("/train/test", j);
            int letter = j + 10;
            testm = getLetterMatrix(name3, letter);

            // 标记合并矩阵中信息的归属
            Matrix mark = new Matrix(ma1.Row + ma2.Row, 1);
			// 代表某个字母或数字的两矩阵合并为一个矩阵
			Matrix final = getTrainMatrix(ma1, ma2, mark);
            List<Matrix> matrixes = new List<Matrix>();
            matrixes.Add(final);
            matrixes.Add(mark);
            matrixes.Add(testm);
            return matrixes;
		}

		/// <summary>
		/// 把一个代表某个数字或字母的文件夹中的全部txt信息存入一个矩阵中
		/// </summary>
		/// <returns>返回代表一个数字或字母全部训练txt信息的矩阵</returns>
		/// <param name="folderName">存储字母或数字全部训练txt的文件夹名</param>
		/// <param name="letter">标识该数字或字母的优先级，用于比较</param>
		public static Matrix getLetterMatrix(string folderName, int letter)
		{
			string[] filePath = Directory.GetFiles(Environment.CurrentDirectory + folderName, "*.txt");
			int row = filePath.Length;

			Matrix matrix = new Matrix(row, COL);
			matrix.Flag = letter;
			List<int[]> txtList = new List<int[]>();

			foreach (string path in filePath)
			{
				StreamReader reader = new StreamReader(path);
				List<int[]> list = new List<int[]>();

				// 读取一个txt中的数据，每一行的数据存在一个int数组中，整个txt数据存在list中
				while (!reader.EndOfStream)
				{
					string[] line = reader.ReadLine().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
					int[] arr = new int[line.Length];
					for (var i = 0; i < line.Length; i++)
						arr[i] = Convert.ToInt32(line[i]);
					list.Add(arr);
				}
				// 将list的中的一个txt的全部信息放入一个int数组中
				int[] txt = new int[COL];
				int k = 0;
				for (int i = 0; i < list.Count; i++)
				{
					for (int j = 0; j < list[i].Length; j++)
					{
						txt[k] = list[i][j];
						k++;
					}
				}
				txtList.Add(txt);
			}

			// 将文件夹中所有的txt含有的信息存储进一个矩阵中
			for (int i = 0; i < matrix.Row; i++)
			{
				for (int j = 0; j < matrix.Col; j++)
				{
					matrix[i, j] = txtList[i][j];
				}
			}
			return matrix;
		}

		/// <summary>
		/// 两个矩阵合并为一个矩阵，并生成一个标记矩阵标示其中信息所属的原矩阵
		/// </summary>
		/// <returns>合并后的新矩阵</returns>
		/// <param name="a">待合并矩阵</param>
		/// <param name="b">待合并矩阵</param>
		/// <param name="mark">指向标记矩阵的引用</param>
		public static Matrix getTrainMatrix(Matrix a, Matrix b, Matrix mark)
		{
			int row = a.Row + b.Row;
			Matrix matrix = new Matrix(row, COL);

			// 为标记矩阵赋值
			if (a.Flag > b.Flag)
			{
				int k = 0;
				for (int i = 0; i < a.Row; i++)
				{
					mark[k, 0] = 1;
					k++;
				}
				for (int j = 0; j < b.Row; j++)
				{
					mark[k, 0] = -1;
					k++;
				}
			}
			else
			{
				int k = 0;
				for (int i = 0; i < b.Row; i++)
				{
					mark[k, 0] = 1;
					k++;
				}
				for (int j = 0; j < a.Row; j++)
				{
					mark[k, 0] = -1;
					k++;
				}
			}

			// 矩阵合并为一个矩阵
			int m;
			for (m = 0; m < a.Row; m++)
			{
				for (int n = 0; n < COL; n++)
				{
					matrix[m, n] = a[m, n];
				}
			}
			for (int v = 0; v < b.Row; v++)
			{
				for (int n = 0; n < COL; n++)
				{
					matrix[m, n] = b[v, n];
				}
				m++;
			}
			return matrix;
		}
	}
}
