﻿using System;
using System.Collections.Generic;
/**
 * 作者：隗泽浩
 * 本类为KNN算法实现
 * 本类运用的是KNN算法中根据给定半径内中数量最多的某类作为识别类
 */
namespace SVM
{
	public class KNN
	{
		private List<Tuple<int, int>> labels = new List<Tuple<int,int>>();
		private List<Matrix> trains;
		private int k;
		private Matrix matrix;
		private int[] mark = new int[36];

		public KNN(int _k, List<Matrix> matrixs, Matrix ma)
		{
			k = _k;
			trains = matrixs;
			matrix = ma;
		}

		public int Classify()
		{	 	
			for (int i = 0; i < trains.Count; i++)
			{
				double distance = 0;
				for (int j = 0; j < matrix.Col; j++)
				{
					distance = distance + (System.Math.Abs(matrix[0, j] - trains[i][0,j]));
				}
				// 二元组，item1 类别，item2 距离
				labels.Add(new Tuple<int,int>(trains[i].Flag, (int)distance));
			}

			// 计算每一类在指定距离内的数量
			foreach(Tuple<int,int> i in labels)
			{
				if (i.Item2 < k)
					mark[i.Item1]++;
			}

			// 选出被识别的类
			int max = mark[0];
			int pos = 0;
			for (int i = 0; i < 36; i++)
			{
				if (max < mark[i])
				{
					max = mark[i];
					pos = i;
				}
			}
			return pos;
		}
	}
}
