﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace SVM
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void OpenPic_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDia = new OpenFileDialog();
            while (fileDia.ShowDialog() == DialogResult.OK)
            {
                //用户选择的文件的后缀名
                string extension = Path.GetExtension(fileDia.FileName);
                //允许选择的图片格式
                string[] patterns = new string[] { ".jpg", ".jpeg", ".png" };
                if (!((IList<string>)patterns).Contains(extension))
                {
                    MessageBox.Show("仅能选择jpg、jpeg、png格式的图片");
                }
                else if(!fileDia.CheckFileExists)
                {
                    MessageBox.Show("该文件不存在");
                }
                else
                {
                    //显示图片
                    this.pictureBox1.ImageLocation = fileDia.FileName;
                    fileDia.Dispose();
                    break;
                }
            }
        }

        private void ShowPicName_Click(object sender, EventArgs e)
        {
            //显示图中字母
            if (this.pictureBox1.ImageLocation == null)
            {
                MessageBox.Show("未选择图片");
            }

            else if (true == knnButton.Checked)
            {
                ShowLetters.Enabled = false;
                PicName.Text = IdentifyCodeForKNN.identifyLetter("pic/" + Path.GetFileName(this.pictureBox1.ImageLocation));
                ShowLetters.Enabled = true;
            }

            else if (true == SVMButton.Checked)
            {
                ShowLetters.Enabled = false;
                PicName.Text = IdentifyCode.identifyLetter("pic/"+ Path.GetFileName(this.pictureBox1.ImageLocation), kernelType.lin, false);
                ShowLetters.Enabled = true;
            }
            else
            {
                MessageBox.Show("请选择一种算法");
            }
        }
    }
}
