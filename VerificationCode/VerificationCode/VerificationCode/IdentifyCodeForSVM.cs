﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
/**
 * 作者：王宾鲁 隗泽浩
 * 本类用于运行SVM算法
 */
namespace SVM
{
	public class IdentifyCode
	{
		public static string identifyLetter(string path, kernelType kT, bool oneToOne)
		{
            if (!oneToOne)
            {
                FileStream filews = new FileStream("ws_lin_oneall.txt", FileMode.Open);
                StreamReader readerws = new StreamReader(filews, Encoding.Default);
                FileStream filebs = new FileStream("bs_lin_oneall.txt", FileMode.Open);
                StreamReader readerbs = new StreamReader(filebs, Encoding.Default);
                Matrix ws = new Matrix(36, 625);
                Matrix bs = new Matrix(36, 1);
                for (int i = 0; i < bs.Row; i++)
                {
                    string textws = readerws.ReadLine();
                    string textbs = readerbs.ReadLine();
                    bs[i, 0] = double.Parse(textbs);
                    string[] texts = textws.Split(' ');
                    for (int j = 0; j < ws.Col; j++)
                    {
                        ws[i, j] = double.Parse(texts[j]);
                    }
                }
                ImgGray img = new ImgGray();
                Matrix matrix = img.processImg(path);
                Cutter cutter = new Cutter(25);
                List<Matrix> list = new List<Matrix>();
                foreach (Matrix mat in cutter.Cut(matrix))
                {
                    list.Add(Rotate.chooseRotate(mat, 2, Math.PI / 18));
                }
                List<string> code = new List<string>();
                foreach (Matrix mat in list)
                {
                    int[] points = new int[36];
                    Matrix colmat = adjustMatrix(mat);
                    double max = -1000;
                    int finalletter = 0;
                    for(int i = 0; i < 36; i++)
                    {
                        if ((ws.getRow(i) * colmat).ToDouble() + bs[i, 0] > max)
                        {
                            max = (ws.getRow(i) * colmat).ToDouble() + bs[i, 0];
                            finalletter = i;
                            Console.WriteLine("***{0}***{1}***", convertToLetter(i), max);
                        }
                    }
                    Console.WriteLine("--{0}--", convertToLetter(finalletter));
                    if (max < 0)
                    {
                        code.Add(convertToLetter(27));
                    }
                    else
                    {
                        code.Add(convertToLetter(finalletter));
                    }
                }
                filews.Close();
                filebs.Close();
                string result = code[0] + code[1] + code[2] + code[3];
                return result;
            }
            else
            {
                if (kT == kernelType.lin)
                {
                    FileStream filews = new FileStream("ws_lin.txt", FileMode.Open);
                    StreamReader readerws = new StreamReader(filews, Encoding.Default);
                    FileStream filebs = new FileStream("bs_lin.txt", FileMode.Open);
                    StreamReader readerbs = new StreamReader(filebs, Encoding.Default);
                    Matrix ws = new Matrix(630, 625);
                    Matrix bs = new Matrix(630, 1);
                    for (int i = 0; i < bs.Row; i++)
                    {
                        string textws = readerws.ReadLine();
                        string textbs = readerbs.ReadLine();
                        bs[i, 0] = double.Parse(textbs);
                        string[] texts = textws.Split(' ');
                        for (int j = 0; j < ws.Col; j++)
                        {
                            ws[i, j] = double.Parse(texts[j]);
                        }
                    }
                    ImgGray img = new ImgGray();
                    Matrix matrix = img.processImg(path);
                    Cutter cutter = new Cutter(25);
                    List<Matrix> list = new List<Matrix>();
                    foreach (Matrix mat in cutter.Cut(matrix))
                    {
                        list.Add(Rotate.chooseRotate(mat, 2, Math.PI / 18));
                    }
                    List<string> code = new List<string>();
                    foreach (Matrix mat in list)
                    {
                        int[] points = new int[36];
                        Matrix colmat = adjustMatrix(mat);
                        for (int i = 0; i < 36; i++)
                        {
                            for (int j = i + 1; j < 36; j++)
                            {
                                double total = (ws.getRow((70 * i - i * i) / 2 + j - i - 1) * colmat).ToDouble() + bs[(70 * i - i * i) / 2 + j - i - 1, 0];
                                if (total > 0.5 && total < 2)
                                {
                                    points[i]++;
                                }
                                else if (total > -2 && total < -0.5)
                                {
                                    points[j]++;
                                }
                            }
                        }

                        int index = 0;
                        int max = 0;
                        for (int i = 0; i < 36; i++)
                        {
                            if (max < points[i])
                            {
                                max = points[i];
                                index = i;
                            }
                        }
                        Console.WriteLine(max);
                        code.Add(convertToLetter(index));
                    }
                    filews.Close();
                    filebs.Close();
                    string result = code[0] + code[1] + code[2] + code[3];
                    return result;
                }
                else
                {
                    FileStream fileas = new FileStream("history/10elementGause/as_rbf.txt", FileMode.Open);
                    StreamReader readeras = new StreamReader(fileas, Encoding.Default);
                    FileStream filebs = new FileStream("history/10elementGause/bs_rbf.txt", FileMode.Open);
                    StreamReader readerbs = new StreamReader(filebs, Encoding.Default);
                    Matrix alphas = new Matrix(630, 20);
                    Matrix bs = new Matrix(630, 1);
                    for (int i = 0; i < bs.Row; i++)
                    {
                        string textas = readeras.ReadLine();
                        //string textws = readerws.ReadLine();
                        string textbs = readerbs.ReadLine();
                        bs[i, 0] = double.Parse(textbs);
                        string[] texts = textas.Split(' ');
                        for (int j = 0; j < alphas.Col; j++)
                        {
                            alphas[i, j] = double.Parse(texts[j]);
                        }
                    }
                    ImgGray img = new ImgGray();
                    Matrix matrix = img.processImg(path);
                    Cutter cutter = new Cutter(25);
                    List<Matrix> list = new List<Matrix>();
                    foreach (Matrix mat in cutter.Cut(matrix))
                    {
                        list.Add(Rotate.chooseRotate(mat, 2, Math.PI / 18));
                    }
                    List<string> code = new List<string>();
                    foreach (Matrix mat in list)
                    {
                        int[] points = new int[36];
                        Matrix colmat = adjustMatrix(mat);
                        for (int i = 0; i < 36; i++)
                        {
                            for (int j = i + 1; j < 36; j++)
                            {
                                List<Matrix> matrixes = LetterMatrixForTrain.GetTrainByTwoMatrix(i, j);
                                Matrix data = matrixes.ElementAt(0);
                                double fresult = (alphas.getRow((70 * i - i * i) / 2 + j - i - 1) * OptStruct.kernelTrans(data, colmat.Transpose(), kernelType.rbf, 10)).ToDouble() + bs[(70 * i - i * i) / 2 + j - i - 1, 0];
                                if (fresult > 1)
                                {
                                    points[i]++;
                                }
                                else if (fresult < -1)
                                {
                                    points[j]++;
                                }
                            }
                        }

                        int index = 0;
                        int max = 0;
                        for (int i = 0; i < 36; i++)
                        {
                            if (max < points[i])
                            {
                                max = points[i];
                                index = i;
                            }
                        }
                        Console.WriteLine(max);
                        code.Add(convertToLetter(index));
                    }
                    fileas.Close();
                    filebs.Close();
                    string result = code[0] + code[1] + code[2] + code[3];
                    return result;
                }
            }
		}

        public static Matrix adjustMatrix(Matrix matrix)
        {
            Matrix ma = new Matrix(matrix.Row * matrix.Col, 1);
            int k = 0;
            for (int i = 0; i < matrix.Row; i++)
            {
                for (int j = 0; j < matrix.Col; j++)
                {
                    ma[k, 0] = matrix[i, j];
                    k++;
                }
            }
            return ma;
        }

		public static string convertToLetter(int number)
		{
            if (number < 10)
            {
                return number.ToString();
            }
            else
            {
                return ((char)('A' + number - 10)).ToString();
            }
		}
	}
}
